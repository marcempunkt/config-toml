mod config;
use config::Config;

fn main() {
    let config: Config = Config::new();

    println!("{:#?}", config);
}
